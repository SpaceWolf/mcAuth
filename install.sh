#/bin/bash

#This script will create an mcAuth installation in a given users homefolder
# under /home/[user]/mcAuth/
# this script will also download Minecraft.jar to this directory
#After installation, run mcauth --new-profile. This will prompt for a username and password and saved to redential location

#Usage:
# ./install.sh [username]


# Exit script on error
set -e

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi


echo INSTALLING TO /home/$1/, ENTER TO CONTINUE
read a

rm -rf /home/$1/mcAuth/
mkdir /home/$1/mcAuth/
wget https://gitlab.com/SpaceWolf/mcAuth/raw/master/mcAuth.py -O /home/$1/mcAuth/mcAuth.py
chmod +x /home/$1/mcAuth/mcAuth.py
chown $1:$1 /home/$1/mcAuth/ -R

wget http://s3.amazonaws.com/Minecraft.Download/launcher/Minecraft.jar -O /home/$1/mcAuth/Minecraft.jar
chown $1:$1 /home/$1/mcAuth/Minecraft.jar

mkdir -p /home/$1/.minecraft/
chown $1:$1 /home/$1/.minecraft/ -R

apt-get install python3-pip
pip3 install simplejson
pip3 install requests

su -l $1 -s /bin/bash -c "/home/$1/mcAuth/mcAuth.py --new-profile"