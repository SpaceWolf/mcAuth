mcAuth
======
This little program is used to log a user into the Minecraft launcher, without the user knowing the username or password. Usefull for kiosk-like applications: https://imgur.com/a/sNP8Q

usage: mcAuth.py [-h] [--ct=CLIENTTOKEN] [--new-profile] [--cleanslate] [--validate] [--refresh] [--getmojangid]
	Run without parameters for normal execution
	Mojang refresh endpoint seems to be bugged, does not work

Required libraries:
- simplejson
- requests


Installation
(see install.sh)
- Bring install.sh with you on a usb or something
- execute "./install.sh [user]"
- when prompted enter username and password
- minecraft can now be started by executing /home/[user]/mcAuth/mcAuth.py