#!/usr/bin/python3

import sys
import getopt
import requests
import simplejson as json
import uuid
import os
import logging

proxy = {
    #'http': 'http://pg-webserver:30264'
}

url = 'https://authserver.mojang.com'
save_location = os.path.expanduser('~') + '/.minecraft/launcher_profiles.json'
cred_location = os.path.expanduser('~') + '/mcAuth/cred.json'
log_location = os.path.expanduser('~') + '/mcAuth/mcAuth.log'
minecraft_bin_location = os.path.expanduser('~') + "/mcAuth/Minecraft.jar"

logging.basicConfig(filename=log_location, level=logging.DEBUG)
logging.debug('\n\n***Starting script!')


#Adds dashes to uuid
def dash(string):
    return string[0:8] + '-' + string[8:12] + '-' + string[12:16] + '-' + string[16:20] + '-' + string[20:]

#removes dashes
def unDash(string):
    return string.replace('-', '')

#Finds out if string is dashed
def isDashed(string):
    if string[8] == '-':
        return True
    else:
        return False

class Login:
    def __init__(self):
        self.loadcred()
        self.credLoaded = False
        self.profileLoaded = False
        self.authenticated = False
        self.clientToken = '' #Always dashed, except for bug in /authenticate endpoint
        self.accessToken = '' #Always dashed
        self.profileIdentifier = '' #Always dashed except for in selected_profile
        self.playerName = ''
        self.username = ''
        self.password = ''
        self.mojangid = ''
    
    #load internal credential file
    def loadcred(self):
        try:
            if not os.path.isfile(cred_location):
                logging.error('Could not find cred.json file...')
                self.credLoaded = False
                return False

            f_obj = open(cred_location, 'r+')
            userpass = json.loads(f_obj.read())
            f_obj.close()
            self.username = userpass['username']
            self.password = userpass['password']
            self.mojangid = userpass['mojangid']

            logging.debug('Loaded credentials: ' + str(userpass))
            self.credLoaded = True
        except:
            logging.error('Unknown error on credential loading!')
            self.credLoaded = False

    def savecred(self):
        try:
            userpass = {}
            userpass['username'] = self.username
            userpass['password'] = self.password
            userpass['mojangid'] = self.mojangid #undashed
            f_obj = open(cred_location, 'w+')
            f_obj.write(json.dumps(userpass))
            f_obj.close()
            logging.debug('Saved credential file.')
        except:
            logging.error("Fatal error on credential saving...")

    #Mojangid is needed in launcher_profiles.json
    def getmojangid(self):  
        #Need a valid accessToken for this to work
        if not self.authenticated and not self.profileLoaded:
            logging.error('No available accessToken, can\'t get mojangid')
            return False

        headers = {'Authorization': 'Bearer ' + self.accessToken}
        response = requests.get('https://api.mojang.com/user', headers=headers, proxies=proxy)
        if response.status_code != 200:
            logging.error('Could not get mojangid: ' + response.text)
        else:
            self.mojangid = json.loads(response.text)['id']
            logging.debug('Got mojangid!: ' + self.mojangid)

    def authenticate(self):
        if not self.credLoaded:
            logging.error("Can't authenticate without login credentials!")
            return False;

        param = {
            "agent": {
                "name": "Minecraft",
                "version": 1
            },
            "username": self.username,
            "password": self.password
        }

        #If we have a clienttoken already: set it, otherwise generate one and set it.
        if self.clientToken != '':
            param["clientToken"] = self.clientToken
            logging.debug('Authenticating using existing clientToken: ' + self.clientToken)
        else:
            self.clientToken = uuid.uuid4().urn[9:]
            param['clientToken'] = self.clientToken
            logging.debug('No clientToken found, new clientToken: ' + self.clientToken)

        #Send auth request
        response = requests.post(url + "/authenticate", data=json.dumps(param), proxies=proxy)

        if response.status_code != 200:
            # throw error
            logging.error('Could not authenticate! ' + response.text)
            self.authenticated = False
        else:
            jsonResponse = json.loads(response.text)

            self.accessToken = jsonResponse['accessToken']
            logging.debug('Received accessToken: ' + jsonResponse['accessToken'])

            #Receive as dashed or undashed if clienttoken was not supplied on initial request
            # Necessary because of buggy minecraft api
            if isDashed(jsonResponse['clientToken']):
                self.clientToken = jsonResponse['clientToken']
            else:
                self.clientToken = dash(jsonResponse['clientToken'])
            logging.debug('Received clientToken: ' + jsonResponse['clientToken'])

            self.profileIdentifier = jsonResponse['availableProfiles'][0]['id']
            self.playerName = jsonResponse['availableProfiles'][0]['name']

            logging.debug('Successful new authentication (new clienttoken: ' + self.clientToken + ')')
            self.authenticated = True

    #Not working. Bug in Minecraft refresh api?
    #Requests new accessToken. Maybe a changed playerName too?
    def refresh(self):
        param = {
            "accessToken": self.accessToken,
            "clientToken": self.clientToken,
            "selectedProfile": {
                "id": self.profileIdentifier,
                "name": self.playerName
            }
        }
        response = requests.post(url + '/refresh', data=json.dumps(param), proxies=proxy)
        if response.status_code != 200:
            # throw error
            logging.error('Could not refresh!')
            self.authenticated = False
        else:
            logging.debug('Successful refresh! New clientToken: ' + jsonResponse['clientToken'] + ' playerName: ' + jsonResponse['selectedProfile']['name'])
            jsonResponse = json.loads(response.text)

            self.accessToken = jsonResponse['accessToken']
            self.clientToken = jsonResponse['clientToken']  #receive undashed
            self.profileIdentifier = jsonResponse['selectedProfile']['id']
            self.playerName = jsonResponse['selectedProfile']['name']
            self.authenticated = True
            self.validAccessToken = True

    #Check wheter current session is valid
    def validate(self):
        if not self.authenticated and not self.profileLoaded:
            logging.error('No available accessToken, can\'t validate')
            self.authenticated = False
            return False

        logging.debug('Validating session with accessToken: ' + self.accessToken + ' and clientToken: ' + self.clientToken)

        param = {
            "accessToken": self.accessToken,
            "clientToken": self.clientToken #Dashed
        }
        response = requests.post(url + '/validate', data=json.dumps(param), proxies=proxy)

        if response.status_code != 204:
            self.authenticated = False
            logging.error('Token could not be validated!')
            return False;
        else:
            self.authenticated = True
            logging.debug('Token valid!')
            return True;

    def saveauth(self):
        logging.debug('Saving profile data to file.')
        data = {
            "profiles": {   
                self.playerName: {
                    "name": self.playerName
                }
            },
            "selectedProfile": self.playerName,
            "clientToken": self.clientToken,    #save as dashed
            "authenticationDatabase": {
                self.profileIdentifier: {
                    "username": self.username,
                    "accessToken": self.accessToken,
                    "userid": self.mojangid,
                    "uuid": dash(self.profileIdentifier),
                    "displayName": self.playerName
                }
            },
            "selectedUser": self.profileIdentifier,
            "launcherVersion": {
                "name": "1.6.61",
                "format": 18
            }
        }

        #Make the .minecraft folder if needed
        os.makedirs(os.path.dirname(save_location), exist_ok=True)

        #Write new launcher_profiles.json
        f_obj = open(save_location, "w")
        f_obj.write(json.dumps(data))
        f_obj.close()

        #Save a copy for debug purposes
        f_obj = open(save_location+'.generated', "w")
        f_obj.write(json.dumps(data))
        f_obj.close()

    #Loads launcher_profiles.json file
    def loadauth(self):
        try:
            logging.debug('Loading profile file.')

            f_obj = open(save_location, "r")
            loaded = json.loads(f_obj.read())
            f_obj.close()

            logging.debug('Loaded clientToken: ' + loaded['clientToken'])

            self.profileIdentifier = loaded['selectedUser']
            self.clientToken = loaded['clientToken']
            self.accessToken = loaded['authenticationDatabase'][self.profileIdentifier]['accessToken']
            self.mojangid = loaded['authenticationDatabase'][self.profileIdentifier]['userid']
            self.playerName = loaded['authenticationDatabase'][self.profileIdentifier]['displayName']

            #Legacy support old mcAuth
            if self.mojangid == '':
                self.getmojangid()

            logging.debug('Loaded profile data for user: ' + self.playerName + ' ' + self.username + ' ' + self.mojangid)
            self.profileLoaded = True
            return True;

        except Exception as e:
            logging.error('Unable to load profile file... error: ' + str(e))
            self.profileLoaded = False
            return False;

    #Reauth and begin with clean self-generated launcher_profiles.json
    #Assumes cred_file.json is loaded
    def cleanslate(self):
        try:
            logging.debug('Cleanslate...')
            self.loadcred()
            self.clientToken = ''
            self.authenticate()
            self.saveauth()
        except:
            logging.exception('Failed to authencticate and save :(, not saving.')


#Default bahaviour:
# - Load credentials
# - Load profile file
# - Validate wheter loaded profile is valid, renew if needed
# - ALWAYS execute minecraft binary
def defaultrun():
    logging.debug('Default run...')
    
    try:
        obj.loadcred()
        #Successfully loaded saved credentials?
        if not obj.credLoaded:
            logging.exception('Unable to load credential file, giving up...')
        else:
            obj.loadauth()
            if not obj.profileLoaded:
                logging.error("Could not load profile file! Reauthenticating...")
                obj.cleanslate()
                logging.debug('Cleanslated')
            else:
                obj.validate()
                if not obj.authenticated:
                    logging.error('Profile does not seem to be authenticated! reauthenticating...')
                    obj.authenticate()
                    obj.saveauth()
    finally:
        logging.error('sys exec info: ' + str(sys.exc_info()[0]))
            
        logging.debug('Attempting minecraft launch...')
        os.system('java -jar ' + minecraft_bin_location)
        logging.debug('Launcher seems to have been closed!')


#####################################################################################################

#####################################################################################################

helpstring = 'usage: mcAuth.py [-h] [--new-profile] [--cleanslate] [--validate] [--refresh] [--getmojangid]\n    Run without parameters for normal execution'

try:
    opts, args = getopt.getopt(sys.argv[1:],"h", ['new-profile', 'cleanslate', 'cs', 'validate', 'refresh', 'getmojangid'])
except getopt.GetoptError:
    print("Found incorrect parameter!")
    print(helpstring)
    sys.exit(2)

obj = Login()
for opt, arg in opts:
    if opt == '-h':
        print(helpstring)
        sys.exit()

    if opt in ('--new-profile'):
        print('Username: ')
        obj.username = input()
        print('Password: ')
        obj.password = input()

        obj.credLoaded = True

        obj.authenticate()
        if obj.authenticated:
            obj.getmojangid()
            obj.savecred()
            obj.saveauth()
            logging.debug('Seem to have authenticated!')
        else:
            logging.debug('Incorrect username or password')
        sys.exit()
    elif opt in ('--getmojangid'):
        obj.loadauth()  #Need a valid accessToken
        obj.loadcred()  #Data to insert mojangid into

        obj.getmojangid()
        
        obj.saveauth()  #Save mojangid to profiles file
        obj.savecred() #Keep a copy of mojangid in credential file

        sys.exit()
    elif opt in ('--cleanslate', '--cs'):
        obj.cleanslate()
        sys.exit()
    elif opt in ('--validate'):
        obj.loadauth() #Load profile data
        obj.validate()
        sys.exit()
    elif opt in ('--refresh'):
        obj.loadauth()
        obj.refresh()
        obj.saveauth() 
        logging.debug('New accessToken is: ' + obj.accessToken)
        sys.exit()


defaultrun()

logging.info('Script execution came to an end')







